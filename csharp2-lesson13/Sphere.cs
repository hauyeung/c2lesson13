﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace csharp2_lesson13
{
    class Sphere
    {
         // Automatic class properties.
        public double radius { get; set; }
        static readonly double Dimensions = 0;
        
        // Typical constructor used with double, double, double fingerprint.
        public Sphere(double radius)
        {
            this.radius = radius; 

        }

      
        
        // Default constructor, using this to call constructor with double, double, double fingerprint,
        // Note using explicit 0.0 double literal to prevent any forced type conversions, and 
        // placing empty method body curly braces on same line to conserve space
        public Sphere() : this(0.0) { } 
        
        // Private constructor that takes a Sphere as a parameter, used to clone itself.
        private Sphere(Sphere Sphere) : this(Sphere.radius) {} 
        
        // A method to return a cloned version of the current Sphere.
        // Note that cloning is rarely this simple!
        public Sphere Clone() { return new Sphere(this); } 
            
        // Calculate volume of Sphere.
        public double Volume()
        {
            return Math.Pow(this.radius,3)  * (4 / 3) * Math.PI;
        }
    }
}
