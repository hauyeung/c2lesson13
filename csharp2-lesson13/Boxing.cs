using System;
using System.Collections.Concurrent;
using System.Drawing;
using System.Windows.Forms;


namespace Boxing
{
    public partial class Boxing : Form
    {
        private Button boxingButton;
        private Button closeButton;
        private ListBox boxingListBox;
    
        public Boxing()
        {
        InitializeComponent();
        }
        


        private void InitializeComponent()
        {
            this.boxingListBox = new System.Windows.Forms.ListBox();
            this.boxingButton = new System.Windows.Forms.Button();
            this.closeButton = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // boxingListBox
            // 
            this.boxingListBox.FormattingEnabled = true;
            this.boxingListBox.Location = new System.Drawing.Point(13, 25);
            this.boxingListBox.Name = "boxingListBox";
            this.boxingListBox.Size = new System.Drawing.Size(447, 290);
            this.boxingListBox.TabIndex = 0;
            // 
            // boxingButton
            // 
            this.boxingButton.Location = new System.Drawing.Point(126, 349);
            this.boxingButton.Name = "boxingButton";
            this.boxingButton.Size = new System.Drawing.Size(75, 23);
            this.boxingButton.TabIndex = 1;
            this.boxingButton.Text = "Box It";
            this.boxingButton.UseVisualStyleBackColor = true;
            this.boxingButton.Click += new System.EventHandler(this.boxingButton_Click_1);
            // 
            // closeButton
            // 
            this.closeButton.Location = new System.Drawing.Point(273, 349);
            this.closeButton.Name = "closeButton";
            this.closeButton.Size = new System.Drawing.Size(75, 23);
            this.closeButton.TabIndex = 2;
            this.closeButton.Text = "Close";
            this.closeButton.UseVisualStyleBackColor = true;
            this.closeButton.Click += new System.EventHandler(this.closeButton_Click_1);
            // 
            // Boxing
            // 
            this.ClientSize = new System.Drawing.Size(472, 449);
            this.Controls.Add(this.closeButton);
            this.Controls.Add(this.boxingButton);
            this.Controls.Add(this.boxingListBox);
            this.Name = "Boxing";
            this.Text = "Boxing";
            this.ResumeLayout(false);

        }

        private void boxingButton_Click_1(object sender, EventArgs e)
        {
            // Fun with Box.
            boxingListBox.Items.Add("Creating box...");
            Box box = new Box(5, 10, 15);
            boxingListBox.Items.Add("Cloning box as newBox...");
            Box newBox = box.Clone();
            boxingListBox.Items.Add("Do box and newBox reference same object? " + ReferenceEquals(box, newBox));
            boxingListBox.Items.Add("Setting box to null...");
            box = null;
            boxingListBox.Items.Add("Is box null? " + (box == null));
            boxingListBox.Items.Add("Is newBox null? " + (newBox == null));
            boxingListBox.Items.Add("Volume : " + newBox.Volume());
            // Fun with SquareBox.
            boxingListBox.Items.Add(""); // Empty line
            boxingListBox.Items.Add("Creating squareBox...");
            SquareBox squareBox = new SquareBox(20);
            SquareBox newSquareBox = squareBox.Reference();
            boxingListBox.Items.Add("Do squareBox and newSquareBox reference same object? " + ReferenceEquals(squareBox, newSquareBox));
            boxingListBox.Items.Add("Setting squareBox to null...");
            squareBox = null;
            boxingListBox.Items.Add("Is squareBox null? " + (squareBox == null));
            boxingListBox.Items.Add("Is newSquareBox null? " + (newSquareBox == null));

            //Sphere
            boxingListBox.Items.Add(""); // Empty line
            boxingListBox.Items.Add("Creating Sphere...");
            csharp2_lesson13.Sphere s = new csharp2_lesson13.Sphere(20);
            csharp2_lesson13.Sphere news = s.Clone();
            boxingListBox.Items.Add("Volume of sphere "+ s.Volume());
            boxingListBox.Items.Add("Is s null? " + (squareBox == null));
            boxingListBox.Items.Add("Is news null? " + (newSquareBox == null));
        }

        private void closeButton_Click_1(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}

